/*
 * Algebra labs.
 */

package com.example.demo.persistence;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.demo.domain.MusicItem;

public class JpaItemRepository implements ItemRepository {
	
	@PersistenceContext
	private EntityManager em;

	public MusicItem get(Long id) {
		return em.find(MusicItem.class,id);
	}

	public void persist(MusicItem item) {
		em.persist(item);
		System.out.println(item + " persisted!");
	}

	public void remove(MusicItem item) {
		em.remove(item);
	}
	
	@Override
	public Collection<MusicItem> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<MusicItem> searchByArtistTitle(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}
