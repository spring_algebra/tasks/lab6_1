/*
 * Algebra labs.
 */

package com.example.demo.service;

import java.util.Collection;

import com.example.demo.domain.MusicItem;
import com.example.demo.persistence.ItemRepository;

public class CatalogImpl implements Catalog {

	private ItemRepository itemRepository;

	public CatalogImpl() {}
	
	public CatalogImpl(ItemRepository itemRepository) {
		this.itemRepository=itemRepository;
	}
	public void persistBatch(Collection<MusicItem> items) {
		for (MusicItem musicItem : items) {
			itemRepository.persist(musicItem);	
		}
		System.out.println("If you are seeing this, persistBatch ended normally!");
	}
	
	public void persist(MusicItem item) {
		itemRepository.persist(item);
	}
	public void remove(MusicItem item) {
		itemRepository.remove(item);
	}
	
	public MusicItem findById(Long id) {
		return itemRepository.get(id);
	}

	public Collection<MusicItem> findByKeyword(String keyword) {
		return itemRepository.searchByArtistTitle(keyword);
	}

	@Override
	public int size() {
		return itemRepository.size();
	}
	
	@Override
	public String toString() {
		return "I am a shiny new " + getClass().getName() + " brought to you from Spring" + " but you can just call me " + getClass().getInterfaces()[0] + ".  My itemRepository is " + itemRepository;
	}

}
